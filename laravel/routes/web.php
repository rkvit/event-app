<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EventController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/event',[EventController::class,'index'])->name('event');
Route::post('/addevent',[EventController::class,'createEvent'])->name('addevent');
Route::get('deleteevent/{id}',[EventController::class,'deleteEvent'])->name('deleteEvent');
Route::get('/event/{id}',[EventController::class,'getEvent'])->name('getEvent');
Route::get('/view/{id}',[EventController::class,'viewEvent'])->name('viewEvent');
Route::get('/events',[EventController::class,'getEvents'])->name('getEvents');
Route::post('updateevent',[EventController::class,'updateEvent'])->name('updateevent');