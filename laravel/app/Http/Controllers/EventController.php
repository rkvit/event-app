<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Event;
// use App\Models\EventRecurrence;
use DateTime;
class EventController extends Controller
{
    //
    public function index() {
     return view('event');   
    }
    public function createEvent(Request $request) {
        $validateData=$request->validate([
            'name'=>['required','max:255'],
            'start_date'=>['required'],
            'end_date'=>['required'],
            'type'=>['required'],
            'week'=>['sometimes'],
            'time'=>['required'],
            'frequency'=>['required']
        ]);
        $data=[
            'name'=>$request->name,
            'start_date'=>$request->start_date,
            'end_date'=>$request->end_date,
            'type'=>$request->type,
            'week'=>$request->week,
            'time'=>$request->time,
            'frequency'=>$request->frequency
        ];
        $event=Event::insert($data);
        if($event){
            $message='Record Added Successfully';
        } else {
            $message='Error in Adding Records';
        }
        return view('event',compact('message'));   
       }
       public function getEvents() {
        $event=Event::all();
        return view('events',compact('event'));   
       }
       public function getEvent(Request $request) {
        $event=Event::where('id',$request->id)->get();
       
        return view('event',compact('event'));   
       }
       public function deleteEvent($id){
           $events=Event::find($id);
            $events->delete();
            $event=Event::all();
            if($events){
                $message='Record Deleted Successfully';
            } else {
                $message='Error in Deleting Records';
            }
            return view('events',compact('event','message'));
       }
       public function viewEvent(Request $request) {
        $event=Event::where('id',$request->id)->get();
        
        $sdate=$event[0]->start_date;
        $tdate=$event[0]->end_date;
        $dateTime1=new DateTime($sdate);
        $dateTime2=new DateTime($tdate);
         $diff=$dateTime1->diff($dateTime2);
         $days=$diff->format('%a');
       
        return view('view',compact('event','days'));   
       }
       public function updateEvent(Request $request) {
           
        $data=[
            'name'=>$request->name,
            'start_date'=>$request->start_date,
            'end_date'=>$request->end_date,
            'type'=>$request->type,
            'week'=>$request->week,
            'time'=>$request->time,
            'frequency'=>$request->frequency
        ];
        $events=Event::where('id',$request->id)->update($data);
        if($events){
            $event=Event::all();
            $message='Record Updated Successfully';
        } else {
            $event=Event::all();
            $message='Error in Updating Records';
        }
        return view('events',compact('message','event'));
       }
}
