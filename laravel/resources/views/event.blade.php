<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add Events</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<meta name="csrf-token" content="{{csrf_token()}}">
<script>
function checkChange(e) {
    // alert(e.value);
    
    if(e == "1"){
        // alert(e.value);
        $('#week').hide();
        $('#day').val('');
    } else {
        $('#week').show();
    }
}
function checkDate(e){
    var start_date = $('#start_date').val();
    if(e.value <= start_date ) {
        alert("Event End Date should be greater than Event Start Date");
        $('#end_date').val('');
    }
}
</script>
</head>
<body onload="checkChange({{isset($event[0]->type) ? $event[0]->type :'1'}})">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h1>Add Event</h1>
                <a href="/events" class="btn btn-info pull-right">List Events</a>
                <hr>
               @if(isset($message) && $message)
               <div class="alert alert-info alert-dismissible fade show" role="alert">
  <strong>{{$message }}</strong>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
                @endif
            </div>
        </div>
    
            <form class="form" method="post" action="{{(isset($event[0]->id)) ? '/updateevent' : 'addevent' }}"  >
                <input type="hidden" name="id" value="{{(isset($event[0]->id)) ? $event[0]->id : '' }}">
          {{csrf_field()}}
                <div class="row">
                <div class="form-group col-12">
                    <label> Event Title</label>
                    <input type="text" class="form-control" name="name" required maxlength="255"  value="{{(isset($event[0]->name)) ? $event[0]->name : '' }}"/>
                </div>
                <div class="form-group col-12">
                <label> Start Date</label>

                    <input type="date" class="form-control" required name="start_date" id="start_date" value="{{(isset($event[0]->start_date)) ? $event[0]->start_date:''}}" />
                </div> 
                <div class="form-group col-12">
                <label> End Date</label>
                    <input type="date" class="form-control" required name="end_date" id="end_date" onchange="checkDate(this)" value="{{(isset($event[0]->end_date)) ? $event[0]->end_date:''}}"/>
                </div>
            
                <div class="col-12"><h4>Recurrence</h4><hr></div>
                <div class="form-check col-6 mb-2">
                    <input type="radio" name="type" value="1" id="gridRadios1" onchange="checkChange(this.value);" {{(isset($event[0]->type) && $event[0]->type == '1') ? 'checked':'' }}>
                    <label class="form-check-label" for="gridRadios1">
          
           Repeat
          </label>
</div>
<div class="form-check col-6 mb-2">
                    <input type="radio" name="type" value="2" id="gridRadios2" onchange="checkChange(this.value);" {{(isset($event[0]->type) && $event[0]->type == '2') ? 'checked':''}}>
                    <label class="form-check-label" for="gridRadios2">
          
           Repeat on the
          </label>
</div>
                <div class="form-group col-4">
                    <select name="time" class="form-control" required>
                    <option value="">Please Select</option>
                    <option value="Every" {{(isset($event[0]->time) && $event[0]->time == 'Every') ? 'selected=selected':''}}>Every</option>
                    <option value="Every Other" {{(isset($event[0]->time) && $event[0]->time  == 'Every Other') ? 'selected=selected':''}}>Every Other</option>
                    <option value="Every Third" {{(isset($event[0]->time) && $event[0]->time  == 'Every Third') ? 'selected=selected':''}}>Every Third</option>
                    <option value="Every Fourth"{{(isset($event[0]->time) && $event[0]->time  == 'Every Fourth') ? 'selected=selected':''}}>Every Fourth</option>
                    </select>
                    </div>
                    <div class="form-group col-4" id='week' >
                    <select name="week" class="form-control" id="day">
                    <option value="">Please Select</option>
                    <option value="Sunday" {{(isset($event[0]->week) && $event[0]->week == 'Sunday') ? 'selected=selected':''}}>Sun</option>
                    <option value="Monday" {{(isset($event[0]->week) && $event[0]->week == 'Monday') ? 'selected=selected':''}}>Mon</option>
                    <option value="Tuesday" {{(isset($event[0]->week) && $event[0]->week == 'Tuesday') ? 'selected=selected':''}}>Tues</option>
                    <option value="Wednesday" {{(isset($event[0]->week) && $event[0]->week == 'Wednesday') ? 'selected=selected':''}}>Wed</option>
                    <option value="Thursday"{{(isset($event[0]->week) && $event[0]->week == 'Thursday') ? 'selected=selected':''}} >Thu</option>
                    <option value="Friday"{{(isset($event[0]->week) && $event[0]->week == 'Friday') ? 'selected=selected':''}} >Fri</option>
                    <option value="Saturday"{{(isset($event[0]->week) && $event[0]->week == 'Saturday') ? 'selected=selected':''}} >Sat</option>
                    
                    </select>
                    </div>
                <div class="form-group col-4">
                    <select name="frequency" class="form-control" required>
                    <option value="">Please Select</option>
                    <option value="Day" {{(isset($event[0]->frequency) && $event[0]->frequency == 'Day') ? 'selected=selected':''}}>Day</option>
                    <option value="Week" {{(isset($event[0]->frequency) && $event[0]->frequency == 'Week') ? 'selected=selected':''}}>Week</option>
                    <option value="Month" {{(isset($event[0]->frequency) && $event[0]->frequency == 'Month') ? 'selected=selected':''}} >Month</option>
                    <option value="Year" {{(isset($event[0]->frequency) && $event[0]->frequency == 'Year') ? 'selected=selected':''}} >Year</option>
                    </select>
                    </div>
</div>
<div class="col-12 text-center">
    <button class="btn btn-success" type="submit">{{(isset($event[0]->id)) ? 'Update' : 'Save' }}</button>
</div>
            </form>
           
        </div>
    </div>
</body>
</html>