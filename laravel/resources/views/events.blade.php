<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>List Events</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</head>
<body>
    <div class="container">
        <div class="row">
        <div class="col-12">
                <h1>List Event</h1>
                <a href="/event" class="btn btn-info pull-right">Add New Event</a>
                <hr>
                @if(isset($message) && $message)
               <div class="alert alert-info alert-dismissible fade show" role="alert">
  <strong>{{$message }}</strong>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
                @endif
            </div>
            <div class="col-12 table-responsive">
                
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Dates</th>
                        <th>Occurence</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($event as $key=> $value)
                        <tr>
                            <td>{{$value->id}}</td>
                            <td>{{$value->name}}</td>
                            <td>{{ $value->start_date}} to {{ $value->end_date}}</td>
                           
                            @if($value->type == '1')
                                    <td>{{$value->time}} {{$value->frequency}} </td>
                                @else
                                <td>{{$value->time}} {{$value->week}} {{$value->frequency}}</td>
                            @endif
                           
                            <td>
                            <a href="/view/{{$value->id}}" class="btn btn-primary btn-sm mx-1">View</a>
                            <a href="/event/{{$value->id}}" class="btn btn-primary btn-sm mx-1">Edit</a>
                            <a href="/deleteevent/{{$value->id}}"  class="btn btn-danger btn-sm mx-1">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
        </div>
    </div>
</body>
</html>