
# PREREQUISITE
1. XAMPP or, WAMP for Windows OS click on below link to download
    a) XAMPP: https://www.apachefriends.org/download.html
    b) WAMP: https://www.wampserver.com/en/
2. PHP 7 or Higher
3. Composer: https://getcomposer.org/download/
4. Git Bash: https://git-scm.com/downloads

# Steps After Download
1. After Complete the download Install it into your computer
2. After Completion of Installation.
3. Clone the file from bitbucket repo for that copy the below url
    url: git clone https://rkvit@bitbucket.org/rkvit/event-app.git
4. After Cloning, open the project into visual studio code editor or, atom editor
5. Import the mysql file to PhpMyAdmin in localhost.
6. After Import goto terminal in editor and below command to run the project.
php artisan serv
7. After running this Above command project will run and copy the link and paste to browser url.
8. Hurray now you can access the project.

For any Query or, Issue mail to developer.vishal109@gmail.com

